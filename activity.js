db.users.insertMany([
    {
        "firstName": "Stephen",
        "lastName": "Hawking",
        "age": 76,
        "email": "stephenhawking@mail.com",
        "department": "HR"
    },

    {
        "firstName": "Neil",
        "lastName": "Armstrong",
        "age": 82,
        "email": "neilarmstrong@mail.com",
        "department": "HR"
    },

    {
        "firstName": "Bill",
        "lastName": "Gates",
        "age": 65,
        "email": "billgates@mail.com",
        "department": "Operations"
    },

    {
        "firstName": "Jane",
        "lastName": "Doe",
        "age": 21,
        "email": "janedoe@mail.com",
        "department": "HR"
    }
])

// 2. 
db.users.find(
	{
	    $or: 
	    [
	        {    
	            "firstName": {
	                    $regex: 's',
	                    $options: '$i'
	            }
	        },
	        {    
	            "lastName": {
	                    $regex: 'd',
	                    $options: '$i'
	            }
	        }
	            
	    ]
	},
	{
		"_id": 0,
		"firstName": 1,
		"lastName": 1
	}
)

// 3.

db.users.find({
    $and: 
    [
        {    
            "department": {
                    $regex: 'HR'
            }
        },
        {    
            "age": {
            	$gte: 70
            }
        }

    ]
})

        
// 4.

db.users.find({
    $and: 
    [
        {    
            "firstName": {
                    $regex: 'e',
                    $options: '$i'
            }
        },
        {    
            "age": {
            	$lte: 30
            }
        }

    ]
})